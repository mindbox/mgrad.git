use mgrad::{Gradient, Space};

fn main() {
	let mut output: Vec<u8> = Vec::new();
	let color1 = (255, 0, 0, 255);
	let color2 = (0, 255, 0, 255);
	let color3 = (0, 0, 255, 255);

	let mut grad = Gradient::new();
	grad.push(color1.into()).push(color2.into()).push((color3, 0).into());
	output.append(&mut render("rgb.png", &grad));
	let mut grad = Gradient::new();
	grad.push((color1, 1, Space::Cmy).into())
		.push((color2, 1, Space::Cmy).into())
		.push((color3, 0).into());
	output.append(&mut render("cmy.png", &grad));
	let mut grad = Gradient::new();
	grad.push((color1, 1, Space::Lab).into())
		.push((color2, 1, Space::Lab).into())
		.push((color3, 0).into());
	output.append(&mut render("lab.png", &grad));
	let mut grad = Gradient::new();
	grad.push((color1, 1, Space::Hsl).into())
		.push((color2, 1, Space::Hsl).into())
		.push((color3, 0).into());
	output.append(&mut render("hsl.png", &grad));
	let mut grad = Gradient::new();
	grad.push((color1, 1, Space::Hsv).into())
		.push((color2, 1, Space::Hsv).into())
		.push((color3, 0).into());
	output.append(&mut render("hsv.png", &grad));
	let mut grad = Gradient::new();
	grad.push((color1, 1, Space::Lch).into())
		.push((color2, 1, Space::Lch).into())
		.push((color3, 0).into());
	output.append(&mut render("lch.png", &grad));

	save_png("all.png", 256, 32 * (6), &output);
}

fn render(_file: &str, grad: &Gradient) -> Vec<u8> {
	let mut bitmap: Vec<u8> = Vec::new();
	for c in grad.color_iter(256) {
		bitmap.extend([c.0 as u8, c.1 as u8, c.2 as u8, c.3 as u8]);
	}
	let bitmap = bitmap.repeat(32);
	// save_png(_file, 256, 32, &bitmap);
	bitmap
}

fn save_png(file: &str, width: u32, height: u32, mask: &[u8]) {
	let file = std::fs::File::create(file).unwrap();
	let mut png_encoder = png::Encoder::new(file, width, height);
	png_encoder.set_depth(png::BitDepth::Eight);
	png_encoder.set_color(png::ColorType::Rgba);
	let mut png_writer = png_encoder.write_header().unwrap();
	png_writer.write_image_data(&mask).unwrap();
}
