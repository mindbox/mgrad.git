#[derive(Debug)]
pub struct Polation {
	pub min: f64,
	pub max: f64,
}
impl Polation {
	pub fn new(min: f64, max: f64) -> Self {
		Self { min, max }
	}
	fn size(&self) -> f64 {
		self.max - self.min
	}
	fn polate(&self, value: f64) -> f64 {
		let value = value.clamp(self.min, self.max);
		value - self.min
	}
	fn normalize(&self, value: f64) -> f64 {
		if self.max <= self.min {
			0.0
		} else {
			self.polate(value) / self.size()
		}
	}
	fn unnormalize(&self, value: f64) -> f64 {
		if self.max <= self.min {
			0.0
		} else {
			value * self.size() + self.min
		}
	}
}
pub trait Polatable {
	fn norm(&self, polation: &Polation) -> Self;
	fn unno(&self, polation: &Polation) -> Self;
}
impl Polatable for f64 {
	fn norm(&self, polation: &Polation) -> Self {
		polation.normalize(*self)
	}
	fn unno(&self, polation: &Polation) -> Self {
		polation.unnormalize(*self)
	}
}
#[test]
fn test_polation() {
	let tex = Polation::new(0.0, 255.0);
	let gra = Polation::new(0.0, 500.0);
	let gap = Polation::new(200.0, 300.0);
	assert_eq!(0.5, 127.5.norm(&tex).unno(&gra).norm(&gap))
}
