pub trait Alphar {
	fn apply_alpha(&self) -> ((f64, f64, f64), f64);
}
impl Alphar for (f64, f64, f64, f64) {
	fn apply_alpha(&self) -> ((f64, f64, f64), f64) {
		let alpha = self.3 / 255.0;
		((self.0 * alpha, self.1 * alpha, self.2 * alpha), alpha)
	}
}
impl Alphar for [f64; 4] {
	fn apply_alpha(&self) -> ((f64, f64, f64), f64) {
		(self[0], self[1], self[2], self[3]).apply_alpha()
	}
}
impl Alphar for (f32, f32, f32, f32) {
	fn apply_alpha(&self) -> ((f64, f64, f64), f64){
		(self.0 as f64, self.1 as f64, self.2 as f64, self.3 as f64).apply_alpha()
	}
}
impl Alphar for [f32; 4] {
	fn apply_alpha(&self) -> ((f64, f64, f64), f64) {
		(self[0], self[1], self[2], self[3]).apply_alpha()
	}
}
impl Alphar for (u8, u8, u8, u8) {
	fn apply_alpha(&self) -> ((f64, f64, f64), f64) {
		(self.0 as f64, self.1 as f64, self.2 as f64, self.3 as f64).apply_alpha()
	}
}
impl Alphar for [u8; 4] {
	fn apply_alpha(&self) -> ((f64, f64, f64), f64) {
		(self[0], self[1], self[2], self[3]).apply_alpha()
	}
}
