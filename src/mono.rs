use crate::interpolable::{AngleDecreasing, AngleIncreasing, AngleLonger, AngleShorter, Interpolable};
use color_space::{Cmy, FromRgb, Hsl, Hsv, Lab, Lch, Rgb};

#[derive(Debug, Clone, Copy, Default)]
pub enum Space {
	#[default]
	Rgb,
	Cmy,
	Hsl,
	Hsv,
	Lab,
	Lch,
}
impl Space {
	fn into_space(&self, rgb: &Rgb) -> (f64, f64, f64) {
		match self {
			Space::Rgb => (rgb.r, rgb.g, rgb.b),
			Space::Cmy => {
				let v = Cmy::from_rgb(rgb);
				(v.c, v.m, v.y)
			}
			Space::Hsl => {
				let v = Hsl::from_rgb(rgb);
				(v.h, v.s, v.l)
			}
			Space::Hsv => {
				let v = Hsv::from_rgb(rgb);
				(v.h, v.s, v.v)
			}
			Space::Lab => {
				let v = Lab::from_rgb(rgb);
				(v.l, v.a, v.b)
			}
			Space::Lch => {
				let v = Lch::from_rgb(rgb);
				(v.h, v.l, v.c)
			}
		}
	}
	fn from_space(&self, value: (f64, f64, f64)) -> Rgb {
		match self {
			Space::Rgb => Rgb::new(value.0, value.1, value.2),
			Space::Cmy => Cmy::new(value.0, value.1, value.2).into(),
			Space::Hsl => Hsl::new(value.0, value.1, value.2).into(),
			Space::Hsv => Hsv::new(value.0, value.1, value.2).into(),
			Space::Lab => Lab::new(value.0, value.1, value.2).into(),
			Space::Lch => Lch::new(value.1, value.2, value.0).into(),
		}
	}
}
#[derive(Debug, Clone, Copy, Default)]
pub enum Inter {
	#[default]
	Shorter,
	Longer,
	Increasing,
	Decreasing,
}
impl Inter {
	pub fn at(&self, from: &(f64, f64, f64), to: &(f64, f64, f64), space: Space, polation: f64) -> (f64, f64, f64) {
		match space {
			Space::Rgb | Space::Cmy | Space::Lab => from.at(to, polation),
			Space::Hsl | Space::Hsv | Space::Lch => match self {
				Inter::Shorter => {
					let ret = (AngleShorter::from(from.0), from.1, from.2).at(
						&(AngleShorter::from(to.0), to.1, to.2),
						polation, //
					);
					(ret.0.into(), ret.1, ret.2)
				}
				Inter::Longer => {
					let ret = (AngleLonger::from(from.0), from.1, from.2).at(
						&(AngleLonger::from(to.0), to.1, to.2),
						polation, //
					);
					(ret.0.into(), ret.1, ret.2)
				}
				Inter::Increasing => {
					let ret = (AngleIncreasing::from(from.0), from.1, from.2).at(
						&(AngleIncreasing::from(to.0), to.1, to.2),
						polation, //
					);
					(ret.0.into(), ret.1, ret.2)
				}
				Inter::Decreasing => {
					let ret = (AngleDecreasing::from(from.0), from.1, from.2).at(
						&(AngleDecreasing::from(to.0), to.1, to.2),
						polation, //
					);
					(ret.0.into(), ret.1, ret.2)
				}
			},
		}
	}
}
#[derive(Debug)]
pub struct Mono {
	from: (f64, f64, f64),
	to: (f64, f64, f64),
	inter: Inter,
	space: Space,
}
impl Mono {
	pub fn new(from: Rgb, to: Rgb, space: Space, inter: Inter) -> Self {
		Self {
			from: space.into_space(&from),
			to: space.into_space(&to),
			inter,
			space,
		}
	}
	pub fn at(&self, polation: f64) -> Rgb {
		self.space.from_space(self.inter.at(&self.from, &self.to, self.space, polation))
	}
}

#[derive(Debug)]
pub struct MonoAlpha {
	from: f64,
	to: f64,
}
impl MonoAlpha {
	pub fn new(from: f64, to: f64) -> Self {
		Self { from, to }
	}
	pub fn at(&self, polation: f64) -> f64 {
		self.from.at(&self.to, polation)
	}
}
#[test]
fn test_mono() {
	let mono = Mono::new(
		Rgb::new(255.0, 0.0, 0.0),
		Rgb::new(0.0, 255.0, 0.0),
		Space::default(),
		Inter::default(),
	);
	assert_eq!(mono.at(0.5), Rgb::new(127.5, 127.5, 0.0));
}
