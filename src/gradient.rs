use color_space::Rgb;

use crate::{
	alpha::Alphar,
	polation::{Polatable, Polation},
	Inter, Mono, MonoAlpha, Space,
};

#[derive(Debug, Clone, Copy)]
pub struct Stop {
	pub color: (u8, u8, u8, u8),
	pub distance: u16,
	pub space: Space,
	pub inter: Inter,
}
impl From<((u8, u8, u8, u8), u16, Space, Inter)> for Stop {
	fn from(value: ((u8, u8, u8, u8), u16, Space, Inter)) -> Self {
		Self {
			color: value.0,
			distance: value.1,
			space: value.2,
			inter: value.3,
		}
	}
}
impl From<((u8, u8, u8, u8), u16, Space)> for Stop {
	fn from(value: ((u8, u8, u8, u8), u16, Space)) -> Self {
		Self {
			color: value.0,
			distance: value.1,
			space: value.2,
			inter: Default::default(),
		}
	}
}
impl From<((u8, u8, u8, u8), u16)> for Stop {
	fn from(value: ((u8, u8, u8, u8), u16)) -> Self {
		Self {
			color: value.0,
			distance: value.1,
			space: Default::default(),
			inter: Default::default(),
		}
	}
}
impl From<(u8, u8, u8, u8)> for Stop {
	fn from(value: (u8, u8, u8, u8)) -> Self {
		Self {
			color: value,
			distance: 1,
			space: Default::default(),
			inter: Default::default(),
		}
	}
}
#[derive(Debug)]
pub struct Gradient {
	pub stops: Vec<Stop>,
	pub distance: f64,
}
impl Gradient {
	pub fn new() -> Self {
		Self {
			stops: Vec::new(),
			distance: 0.0,
		}
	}
	pub fn push(&mut self, stop: Stop) -> &mut Self {
		self.distance += stop.distance as f64;
		self.stops.push(stop);
		self
	}
	pub fn mono_iter(&self) -> MonoIter<'_> {
		let mut iter = self.stops.iter();
		MonoIter {
			pos: 0.0,
			last: iter.next(),
			iter,
		}
	}
	pub fn color_iter(&self, width: u32) -> ColorIter<'_> {
		let mut iter = self.mono_iter();
		ColorIter {
			pos: 0,
			width,
			tex: Polation::new(0.0, width.into()),
			gra: Polation::new(0.0, self.distance),
			last: iter.next(),
			iter,
		}
	}
}
#[derive(Debug)]
pub struct MonoIter<'a> {
	pos: f64,
	last: Option<&'a Stop>,
	iter: std::slice::Iter<'a, Stop>,
}
impl<'a> Iterator for MonoIter<'a> {
	type Item = (Mono, MonoAlpha, Polation);
	fn next(&mut self) -> Option<Self::Item> {
		let Some(last) = self.last else {
			return None;
		};
		let next = self.iter.next();
		let ret = if let Some(next) = next {
			let (from_color, from_alpha) = last.color.apply_alpha();
			let (to_color, to_alpha) = next.color.apply_alpha();
			let from_rgb = Rgb::new(from_color.0, from_color.1, from_color.2);
			let to_rgb = Rgb::new(to_color.0, to_color.1, to_color.2);
			(
				Mono::new(from_rgb, to_rgb, last.space, last.inter),
				MonoAlpha::new(from_alpha, to_alpha),
				Polation::new(self.pos, self.pos + last.distance as f64),
			)
		} else {
			let (color, alpha) = last.color.apply_alpha();
			let rgb = Rgb::new(color.0, color.1, color.2);
			(
				Mono::new(rgb, rgb, last.space, last.inter),
				MonoAlpha::new(alpha, alpha),
				Polation::new(self.pos, self.pos),
			)
		};
		self.pos += next.map(|v| v.distance).unwrap_or_default() as f64;
		self.last = next;
		ret.into()
	}
}

pub struct ColorIter<'a> {
	pos: u32,
	width: u32,
	tex: Polation,
	gra: Polation,
	iter: MonoIter<'a>,
	last: Option<(Mono, MonoAlpha, Polation)>,
}
impl<'a> Iterator for ColorIter<'a> {
	type Item = (f64, f64, f64, f64);
	fn next(&mut self) -> Option<Self::Item> {
		if self.pos >= self.width {
			return None;
		}
		let pos = (self.pos as f64).norm(&self.tex).unno(&self.gra);
		self.pos += 1;

		while pos >= self.last.as_ref().map(|v| v.2.max).unwrap_or(0.0) {
			if let Some(next) = self.iter.next() {
				self.last = Some(next);
			} else {
				break;
			}
		}

		if let Some(mono) = self.last.as_ref() {
			let polation = pos.norm(&mono.2);
			let rgb = mono.0.at(polation);
			let alpha = mono.1.at(polation);
			Some((rgb.r, rgb.g, rgb.b, alpha * 255.0))
		} else {
			Some((0.0, 0.0, 0.0, 0.0))
		}
	}
}
#[test]
fn test_gradient() {
	let mut grad = Gradient::new();
	grad.push((128, 0, 0, 255).into())
		.push((0, 128, 0, 255).into())
		.push(((0, 0, 128, 255), 0).into());

	assert_eq!(
		grad.color_iter(16).collect::<Vec<_>>(),
		[
			(128.0, 0.0, 0.0, 255.0),
			(112.0, 16.0, 0.0, 255.0),
			(96.0, 32.0, 0.0, 255.0),
			(80.0, 48.0, 0.0, 255.0),
			(64.0, 64.0, 0.0, 255.0),
			(48.0, 80.0, 0.0, 255.0),
			(32.0, 96.0, 0.0, 255.0),
			(16.0, 112.0, 0.0, 255.0),
			(0.0, 128.0, 0.0, 255.0),
			(0.0, 112.0, 16.0, 255.0),
			(0.0, 96.0, 32.0, 255.0),
			(0.0, 80.0, 48.0, 255.0),
			(0.0, 64.0, 64.0, 255.0),
			(0.0, 48.0, 80.0, 255.0),
			(0.0, 32.0, 96.0, 255.0),
			(0.0, 16.0, 112.0, 255.0),
		]
	);
}
