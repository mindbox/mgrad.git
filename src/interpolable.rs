pub trait Interpolable: Sized + Clone {
	fn at(&self, to: &Self, polation: f64) -> Self;
}
impl Interpolable for f64 {
	#[inline]
	fn at(&self, to: &Self, polation: f64) -> Self {
		if polation.abs() < f64::EPSILON {
			return *self;
		}
		if (1.0 - polation).abs() < f64::EPSILON {
			return *to;
		}
		if (self - to).abs() < f64::EPSILON {
			return *self;
		}
		let v = (*self as f64) * (1.0 - polation) + (*to as f64) * polation;
		v as Self
	}
}
impl<T1: Interpolable, T2: Interpolable, T3: Interpolable> Interpolable for (T1, T2, T3) {
	#[inline]
	fn at(&self, to: &Self, polation: f64) -> Self {
		let t0 = Interpolable::at(&self.0, &to.0, polation);
		let t1 = Interpolable::at(&self.1, &to.1, polation);
		let t2 = Interpolable::at(&self.2, &to.2, polation);
		(t0, t1, t2)
	}
}

// https://www.w3.org/TR/css-color-4/#hue-interpolation
#[derive(Debug, Clone, Copy)]
pub struct AngleShorter(pub f64);
impl From<f64> for AngleShorter {
	fn from(value: f64) -> Self {
		Self(value)
	}
}
impl From<AngleShorter> for f64 {
	fn from(value: AngleShorter) -> Self {
		value.0
	}
}
impl Interpolable for AngleShorter {
	fn at(&self, to: &Self, polation: f64) -> Self {
		let mut a_1 = self.0 - 180.0;
		let mut a_2 = to.0 - 180.0;
		if a_2 - a_1 > 180.0 {
			a_1 += 360.0;
		} else if a_2 - a_1 < -180.0 {
			a_2 += 360.0;
		}
		let diff = a_2 - a_1;
		let add = diff * polation;
		Self((self.0 + add + 360.0) % 360.0)
	}
}

#[derive(Debug, Clone, Copy)]
pub struct AngleLonger(pub f64);
impl From<f64> for AngleLonger {
	fn from(value: f64) -> Self {
		Self(value)
	}
}
impl From<AngleLonger> for f64 {
	fn from(value: AngleLonger) -> Self {
		value.0
	}
}
impl Interpolable for AngleLonger {
	fn at(&self, to: &Self, polation: f64) -> Self {
		let mut a_1 = self.0 - 180.0;
		let mut a_2 = to.0 - 180.0;
		if 0.0 < a_2 - a_1 && a_2 - a_1 < 180.0 {
			a_1 += 360.0;
		} else if -180.0 < a_2 - a_1 && a_2 - a_1 < 0.0 {
			a_2 += 360.0;
		}
		let diff = a_2 - a_1;
		let add = diff * polation;
		Self((self.0 + add + 360.0) % 360.0)
	}
}

#[derive(Debug, Clone, Copy)]
pub struct AngleIncreasing(pub f64);
impl From<f64> for AngleIncreasing {
	fn from(value: f64) -> Self {
		Self(value)
	}
}
impl From<AngleIncreasing> for f64 {
	fn from(value: AngleIncreasing) -> Self {
		value.0
	}
}
impl Interpolable for AngleIncreasing {
	fn at(&self, to: &Self, polation: f64) -> Self {
		let a_1 = self.0 - 180.0;
		let mut a_2 = to.0 - 180.0;
		if a_2 < a_1 {
			a_2 += 360.0;
		}
		let diff = a_2 - a_1;
		let add = diff * polation;
		Self((self.0 + add + 360.0) % 360.0)
	}
}

#[derive(Debug, Clone, Copy)]
pub struct AngleDecreasing(pub f64);
impl From<f64> for AngleDecreasing {
	fn from(value: f64) -> Self {
		Self(value)
	}
}
impl From<AngleDecreasing> for f64 {
	fn from(value: AngleDecreasing) -> Self {
		value.0
	}
}
impl Interpolable for AngleDecreasing {
	fn at(&self, to: &Self, polation: f64) -> Self {
		let mut a_1 = self.0 - 180.0;
		let a_2 = to.0 - 180.0;
		if a_1 < a_2 {
			a_1 += 360.0;
		}
		let diff = a_2 - a_1;
		let add = diff * polation;
		Self((self.0 + add + 360.0) % 360.0)
	}
}
